        ___                   ________
       / _ )___ __________ __/ ___/ (_)__
      / _  / -_) __/ __/ // / /__/ / / _ \
     /____/\__/_/ /_/  \_, /\___/_/_/ .__/
                      /___/        /_/

# Example scripts for the BerryClip GPIO Board

The BerryClip is a simple, cheap and easy to use add-on board for the Raspberry Pi. It plugs 
directly onto the Pi's GPIO header and provides 6 coloured LEDs, 1 Buzzer and 1 Switch. It
can be controlled using any programming language that can manipulate the GPIO pins.

# Python Scripts
* berryclip_01.py - Test LEDs only
* berryclip_02.py - Test Buzzer only
* berryclip_03.py - Test Switch only
* berryclip_04.py - Test LEDs and Switch
* berryclip_05.py - Test LEDs, Buzzer and Switch
* berryclip_06.py - LED sequence
* berryclip_07.py - Dice Simulator
* berryclip_08.py - Reaction time game
* berryclip_09.py - Random LEDs
* berryclip_10.py - Multiple LED sequences in a loop
* berryclip_11.py - Traffic light simulator
* berryclip_12.py - Morse code generator
* berryclip_gui.py

# Other scripts
* berryclip_cpu_01.sh

# PDF User Guide
If you would prefer a PDF version of these instructions with some photos and a circuit
diagram then you download one from :
[https://bitbucket.org/MattHawkinsUK/rpispy-berryclip/downloads/BerryClip%20User%20Guide.pdf](https://bitbucket.org/MattHawkinsUK/rpispy-berryclip/downloads/BerryClip%20User%20Guide.pdf)

# BerryClip articles
Visit my site for more information :
[http://www.raspberrypi-spy.co.uk/berryclip-6-led-add-on-board/](http://www.raspberrypi-spy.co.uk/berryclip-6-led-add-on-board/)