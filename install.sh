#!/bin/bash

# Remove existing files
rm -f berryclip_??.py
rm -f instructions.txt

# Download latest files
wget "http://www.raspberrypi-spy.co.uk/berryclip/6_led/instructions.txt" -w 2
wget "http://www.raspberrypi-spy.co.uk/berryclip/6_led/berryclip_01.py" -w 2
wget "http://www.raspberrypi-spy.co.uk/berryclip/6_led/berryclip_02.py" -w 2
wget "http://www.raspberrypi-spy.co.uk/berryclip/6_led/berryclip_03.py" -w 2
wget "http://www.raspberrypi-spy.co.uk/berryclip/6_led/berryclip_04.py" -w 2
wget "http://www.raspberrypi-spy.co.uk/berryclip/6_led/berryclip_05.py" -w 2
wget "http://www.raspberrypi-spy.co.uk/berryclip/6_led/berryclip_06.py" -w 2
wget "http://www.raspberrypi-spy.co.uk/berryclip/6_led/berryclip_07.py" -w 2
wget "http://www.raspberrypi-spy.co.uk/berryclip/6_led/berryclip_08.py" -w 2
wget "http://www.raspberrypi-spy.co.uk/berryclip/6_led/berryclip_09.py" -w 2
wget "http://www.raspberrypi-spy.co.uk/berryclip/6_led/berryclip_10.py" -w 2
wget "http://www.raspberrypi-spy.co.uk/berryclip/6_led/berryclip_11.py" -w 2
wget "http://www.raspberrypi-spy.co.uk/berryclip/6_led/berryclip_12.py" -w 2

# GUI script
mkdir -p gui
rm -f gui/*.*
wget "http://www.raspberrypi-spy.co.uk/berryclip/6_led/gui/berryclip_gui.py" -w 2
wget "http://www.raspberrypi-spy.co.uk/berryclip/6_led/gui/berryclip_640x480.jpg" -w 2

# Berry Bookies script
mkdir -p berry_bookies
rm -f berry_bookies/*.*
wget "http://www.raspberrypi-spy.co.uk/berryclip/6_led/berry_bookies/berry_bookies.py" -w 2
wget "http://www.raspberrypi-spy.co.uk/berryclip/6_led/berry_bookies/information.txt" -w 2
